import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  funcionarios = [];
  
  nome = 'Sergio';
  dataAniversario = new Date('07/11/1900');
  preco = 2098348578287287287.00;
  troco = 10.00;
  
  aoAdicionar(funcionario){
    
    this.funcionarios.push(funcionario);
  }
}
