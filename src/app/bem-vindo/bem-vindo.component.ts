import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

class Cliente {
  
  nome: string;
  email: string;
  profissao: string;
}

@Component({
  selector: 'app-bem-vindo',
  templateUrl: './bem-vindo.component.html',
  styleUrls: ['./bem-vindo.component.css']
})
export class BemVindoComponent {

  cliente = new Cliente();
  profissoes = ['Programador', 'Analista', 'Test', 'Estag'];
  
  salvar(form: NgForm) {
  
    form.reset();  
  }
}