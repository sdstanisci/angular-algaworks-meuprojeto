import { NgModule } from '@angular/core';

//component
import { AppComponent } from './app.component';
import { BemVindoComponent } from './bem-vindo/bem-vindo.component';
import { FuncionarioCardComponent } from './funcionario-card/funcionario-card.component';
import { FuncionarioFormComponent } from './funcionario-form/funcionario-form.component';

//directives
import { CampoColoridoDirective } from './campo-colorido.directive';

//imports
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    BemVindoComponent,
    FuncionarioCardComponent,
    FuncionarioFormComponent,
    CampoColoridoDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
